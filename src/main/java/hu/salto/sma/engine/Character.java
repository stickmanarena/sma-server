package hu.salto.sma.engine;

import hu.salto.sma.config.Parameters;

public class Character {
    
    public enum Status {
        STANDING, WALKING, FALLING, CLIMBING,
        PUNCHINGLEFT, PUNCHINGRIGHT,
        KICKINGLEFT, KICKINGRIGHT,
        DAMAGEDLEFT, DAMAGEDRIGHT,
        DIEING, DIED,
        JUMP, JUMPLEFT, JUMPRIGHT
    }
    
    public enum Direction {
        LEFT, RIGHT, UP, DOWN, NONE
    }
    
    public double id;
    private double x;
    private double y;        
    private double offsetX;
    private double offsetY;
    public String color;
    private Status state;
    public Direction direction;
    private int maxLife = 40;
    private int life = 40;
    
    /** in steps */
    private int stateSustension;

    public Character(){
        super();
    }
    public Character(double id, double x, double y, String color) {
        super();
        this.id = id;
        this.x = x;
        this.y = y;
        this.color = color;
        this.state = Status.STANDING;
        this.direction = Direction.NONE;
        setMaxLife(40);
        setLife(40);
    }	

    
    protected void step(int[][] map, int blockSize){        
        if (getState() == Status.DIED) return;
        // if out of map, skip
        int mapX = (int)Math.floor(getX()/blockSize) + 1;        
        int mapY = (int)Math.floor(getY()/blockSize) + 2;                
        try {
            int type = map[mapX][mapY];
        } catch (ArrayIndexOutOfBoundsException e) {
            return;
        }
        
        // if there is a sustained state...
        if (getStateSustension() > 0) {
            switch(getState()){
                case DAMAGEDLEFT :                 
                    setX(getX()-Parameters.DAMAGED_KNOCKBACK);                    
                    break;
                case DAMAGEDRIGHT :             
                    setX(getX()+Parameters.DAMAGED_KNOCKBACK);
                    setOffsetY(offsetY);
                    break;
                default:                     
                    break;
            }            
            if (getState() == Status.PUNCHINGLEFT || getState() == Status.KICKINGLEFT){            
                for (Object o : Game.world.getCharactersArray()){
                    Character ch = (Character)o;                    
                    int d = getState() == Status.KICKINGLEFT
                        ? Parameters.KICK_DISTANCE
                        : Parameters.PUNCH_DISTANCE;                
                    int dam = getState() == Status.KICKINGLEFT
                        ? Parameters.KICK_DAMAGE
                        : Parameters.PUNCH_DAMAGE;
                    
                    if (
                            Math.abs(getY()-ch.getY()) < d && getX()-ch.getX() < d &&
                            getX()-ch.getX() > 0 && ch.getLife() > 0 &&
                            ch.getState() != Status.JUMP && ch.getState() != Status.JUMPLEFT && ch.getState() != Status.JUMPRIGHT
                        ){
                        ch.setState(Status.DAMAGEDLEFT);                    
                        ch.setStateSustension(4);
                        ch.setLife(ch.getLife()-dam);
                    }
                }
            }
            else if (getState() == Status.PUNCHINGRIGHT || getState() == Status.KICKINGRIGHT){
                for (Object o : Game.world.getCharactersArray()){
                    Character ch = (Character)o;
                    int d = getState() == Status.KICKINGRIGHT
                        ? Parameters.KICK_DISTANCE
                        : Parameters.PUNCH_DISTANCE;
                    int dam = getState() == Status.KICKINGRIGHT
                        ? Parameters.KICK_DAMAGE
                        : Parameters.PUNCH_DAMAGE;
                    if (
                            Math.abs(getY()-ch.getY()) < d && ch.getX()-getX() < d &&
                            ch.getX()-getX() > 0 && ch.getLife() > 0 &&
                            ch.getState() != Status.JUMP && ch.getState() != Status.JUMPLEFT && ch.getState() != Status.JUMPRIGHT
                        ){
                        ch.setState(Status.DAMAGEDRIGHT);
                        ch.setStateSustension(4);
                        ch.setLife(ch.getLife()-dam);
                    }
                }
            }
            setStateSustension(getStateSustension()-1);
            return;
        }   
        
        // rules for everone
        switch (getState()) {
            // states that was worn off
            case DIEING :
                die();
                break;            
            case DAMAGEDLEFT :                
                setState(Status.STANDING);
                break;
            case DAMAGEDRIGHT :                
                setState(Status.STANDING);
                break;
            case KICKINGLEFT :                
                setState(Status.STANDING);
                break;
            case KICKINGRIGHT:                
                setState(Status.STANDING);
                break;
            case PUNCHINGLEFT :                
                setState(Status.STANDING);
                break;
            case PUNCHINGRIGHT:                
                setState(Status.STANDING);
                break;   
            case FALLING:                
                if (mapY+1<map[mapX].length){      
                    int blockUnder = map[mapX][mapY+1];
                    if (blockUnder == 1 || blockUnder == 111 || blockUnder == 2 || blockUnder == 112){
                        // if ground touched, start walking
                        state = Status.STANDING;
                        direction = Direction.NONE;
                        // snap to the ground
                        setY((mapY-1)*blockSize-4);
                    } else {
                        // else just fall more
                        setY(getY() + Parameters.FALL_SPEED);
                    }
                } 
                break;
            case WALKING:                
                if (getState() != Status.CLIMBING){
                    // snap to the ground
                    setY((mapY-1)*blockSize-4); 
                }
                // check if walked off of console                
                if (mapY+1<map[mapX].length){
                    int blockUnder = map[mapX][mapY+1];
                    if (blockUnder == 0 || blockUnder == 110){
                        state = Status.FALLING;
                        direction = Direction.DOWN;
                    }
                }
                break;
            case CLIMBING:
                // check if climbed too high
                if (mapY<map[mapX].length){
                    int blockUnder = map[mapX][mapY+1];
                    if (blockUnder == 0 || blockUnder == 110){
                        state = Status.FALLING;
                        direction = Direction.DOWN;
                    }    
                }
                break;
            case STANDING :               
                // check if standing in the nowhere
                if (mapY+1<map[mapX].length){
                    int blockUnder = map[mapX][mapY+1];
                    if (blockUnder == 0 || blockUnder == 110){
                        state = Status.FALLING;
                        direction = Direction.DOWN;
                    }
                }
                break;            
            default:
                break;
        }                             
        if (this.getLife() <= 0 && getState() != Status.DIED){
            setState(Status.DIEING);
            setStateSustension(4);            
        }        
                
    };
    
    protected void die() {
        System.out.println("Character dies");
        setState(Character.Status.DIED);        
    }
    
    
    @Override
    public String toString() {
        return "<<PLAYER>> id: " + id +
            " color: " + color +
            " status: " + state +
            " x: " + x +
            " y: " + y                     
        ;
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public Status getState() {
        return state;
    }

    public void setState(Status state) {
        this.state = state;
    }

    public int getStateSustension() {
        return stateSustension;
    }

    public void setStateSustension(int stateSustension) {
        this.stateSustension = stateSustension;
    }

    public int getLife() {
        return life;
    }

    public void setLife(int life) {
        this.life = life;
    }

    public int getMaxLife() {
        return maxLife;
    }    
    public void setMaxLife(int maxLife) {
        this.maxLife = maxLife;
    }

    public double getOffsetX() {
        return offsetX;
    }

    public void setOffsetX(double offsetX) {
        this.offsetX = offsetX;
    }

    public double getOffsetY() {
        return offsetY;
    }

    public void setOffsetY(double offsetY) {
        this.offsetY = offsetY;
    }
    
    
    
}
