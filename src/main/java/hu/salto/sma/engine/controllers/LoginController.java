package hu.salto.sma.engine.controllers;

import hu.salto.sma.engine.Game;
import hu.salto.sma.engine.models.LoggedInInfo;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import hu.salto.sma.engine.Player;

@Controller
public class LoginController {
    @MessageMapping("/anonimlogin")
    @SendTo("/broadcast/loggedin")
    public LoggedInInfo login(double uid) throws Exception {        
        System.out.println("New user has joined with ID " + uid);        
        Player player = new Player(uid, 0, 0, "gray");                
        Game.world.addPlayerToRandomPlace(player);
        return new LoggedInInfo(player, Game.world.getMap());
    }
}
