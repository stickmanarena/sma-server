package hu.salto.sma.engine;

import hu.salto.sma.StickManArenaServerApplication;
import hu.salto.sma.config.Parameters;
import java.util.Collection;

import java.util.Hashtable;

public class World {
    private Hashtable<Double, Character> characters;       // both player and npcs
    private Hashtable<Double, Npc> npcs;
    private Hashtable<Double, Player> players;
    private int w;                                          // px
    private int h;                                          // px
    private String name;
    private int[][] map;
    private int npcIdCount = 1;
    private int blockSize;

    public World(int w, int h, int blockSize, int initNpcCount) {        
        this.w = w;
        this.h = h;
        this.blockSize = blockSize;
        this.name = "Test";
        characters = new Hashtable<Double, Character>();
        players = new Hashtable<Double, Player>();
        npcs = new Hashtable<Double, Npc>();        
        map = MapGenerator.newMap((int)w/blockSize, (int)h/blockSize);
        for (int i=0; i<initNpcCount; i++){
            this.addRandomNpc();
        }
    }

    public void step(){
        if (characters == null) return;
        
        for (Character ch : characters.values()){
            ch.step(map, blockSize);                        
        }
        for (Character ch : characters.values()){
            if (ch.getState() == Character.Status.DIED){             
                Game.world.removeCharacter(ch);
            }
        }
    }
    
    public void addPlayerToRandomPlace(Player p){
        p.setX(Math.random()*w-50);
        p.setY(Math.random()*h-50);
        p.setState(Player.Status.FALLING);
        p.color = "#f8d02e";
        System.out.println(p);
        players.put(p.id, p);
        characters.put(p.id, p);
    }
    public Player getPlayer(double id){
        return players.get(id);
    }
    public void addNpc(Npc n){
        npcs.put(n.id, n);
        characters.put(n.id, n);
    }
    public void addRandomNpc(){
        Npc npc = new Npc(npcIdCount, Math.random()*w-50, Math.random()*h-100, "#70ec28");        
        this.addNpc(npc);
        npcIdCount++;
    }
    public Object[] getCharactersArray(){
        return characters.values().toArray();
    }   
    public Object[] getPlayersArray(){
        return players.values().toArray();
    } 

    public int[][] getMap() {
        return map;
    }

    void removeCharacter(Character ch) {        
        characters.remove(ch.id);        
    }
    
}
