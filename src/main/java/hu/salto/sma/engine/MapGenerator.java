/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package hu.salto.sma.engine;

/**
 *
 * @author gszemes
 */
public class MapGenerator {

    static int[][] newMap(int w, int h){
        int[][] map = new int[w][h];                               
        
        // place platforms
        int prevT;
        for (int i=0; i<w; i++){            
            for (int j=0; j<h; j++){                
                double r = Math.random();
                int t = 0;
                if (j%5 == 4) {
                    if (i==0) {
                        if (r<0.5) t=1;
                    } else {
                        prevT = map[i-1][j];
                        if (prevT == 1){
                            if (r<0.95) t = prevT;
                            else t = 1-prevT;
                        } else {
                            if (r<0.88) t = prevT;
                            else t = 1-prevT;
                        }
                    }
                    map[i][j] = t;
                }
                else map[i][j] = 0;                
            }
        }
        
        // place ladders
        int ladderCount = (int)(w*h/245);
        for (int i=0; i<ladderCount; i++){
            int lx = (int)(Math.random()*w);
            int ly = (int)(Math.random()*h);
            map[lx][ly] = 2;                        
            int upy = ly;
            while (upy-1 >= 0 && map[lx][upy-1]<=0){
                upy--;
                map[lx][upy] = 2;                                                
            }
            if (upy>0) map[lx][upy-1] = 2;
            int downy = ly;
            while (downy+1 < h && map[lx][downy+1]<=0){
                downy++;
                map[lx][downy] = 2;                                                
            }
            if (downy<h && downy+1 < h) map[lx][downy+1] = 2;
        }        
        
        // border it around
        for (int x=0; x<w; x++){
            map[x][0] = 1;
            map[x][h-1] = 1;
        }
        for (int x=0; x<h; x++){
            map[0][x] = 1;
            map[w-1][x] = 1;
        }
        
        return map;                        
    }
}
