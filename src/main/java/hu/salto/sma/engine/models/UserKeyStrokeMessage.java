package hu.salto.sma.engine.models;

public class UserKeyStrokeMessage {
    public double uid;
    public int keyCode;
    public UserKeyStrokeMessage(){}
    public UserKeyStrokeMessage(double uid, int keyCode) {
        super();
        this.uid = uid;
        this.keyCode = keyCode;
    }	    
}
